// Utility to load a fasta file and extract portions of some sequences

package main

import (
    "bufio"
    "flag"
    "fmt"
    "log"
    "os"
    "strconv"
    "strings"

    "bitbucket.org/GottySG36/sequtils"
)

type Coord struct {
    Start   int
    End     int
}

type Sub map[string][]Coord

func NewSub() Sub {
    s := make(Sub, 0)
    return s
}

func (s *Sub) NewCoord(n string, st, e int) {
    if _, ok := (*s)[n]; !ok {
        (*s)[n] = make([]Coord, 0)
    }
    (*s)[n] = append((*s)[n], Coord{Start: st, End: e})
}

func LoadSubseqs(f, sep string, z bool) (Sub, error) {
    var correct int
    if !z { correct = 1 }

    i, err := os.Open(f)
    defer i.Close()
    if err != nil {
        return nil, err
    }
    sub := make(Sub, 0)
    r := bufio.NewScanner(i)
    for r.Scan() {
        line := r.Text()
        if line == "" {continue}
        sl := strings.Split(line, sep)
        if len(sl) != 3 {
            return nil, fmt.Errorf("MissingField : %v", line)
        }
        start, err := strconv.Atoi(sl[1])
        if err != nil {
            return nil, err
        }
        end, err := strconv.Atoi(sl[2])
        if err != nil {
            return nil, err
        }
        if start > end {
            start, end = end, start
        }
        sub.NewCoord(sl[0], start - correct, end - correct)
    }
    return sub, nil
}



var (
    input   = flag.String("i", "", "Input fasta to filter")
    list    = flag.String("seqs", "", "File containing the list of fasta sequences to extract along with a start and end position.")
    output  = flag.String("o", "", "Output file to write filtered fasta")
    zero    = flag.Bool("zero-index", false, `Tells the script that the numbers provided assume zero indexing.
Otherwise, all start and end positions will be shifted to zero by 1 (default).`)

    sep     = flag.String("d", "\t", "Delimiter used in sequence list.")

    synopsis = `Simple utility to extract portions of fasta sequences.`

    uString  = `get-subseqs -i <FILE> -seqs <FILE> -o <FILE> [-d STR] [-zero-index]`
    details  = `This utility is designed to read in a fasta file along with a file listing the sequences to extract
and what partion needs to bes extracted.

The file containing the list of sequences needs three columns (default field delimiter is '\t') :
1. The sequence header;
2. The start position, and;
3. The end position.

Also, depending on how you obtained your ranges, it may be important to provid the flag '-zero-index'.
This is only useful if you consider the first position to be zero and not one. In most cases, it
shouldn't be needed to provide it. If you aren't sure, ignore it. If you get some errors about being
out of range or something, try it. If that also fails, there is either a probleme with your ranges
or this tool.`
)

var Usage = func() {
    fmt.Fprintf(flag.CommandLine.Output(), "%v\n\n", synopsis)
    fmt.Fprintf(flag.CommandLine.Output(), "Usage : %v\n\n", uString)

    fmt.Fprintf(flag.CommandLine.Output(), "Arguments of %s:\n", os.Args[0])
    flag.PrintDefaults()

    fmt.Printf("\nDetailed description :\n%v\n", details)
}

func main() {
    flag.Usage = Usage
    flag.Parse()
    if *input == "" {
        log.Fatalf("Error -:- No input provided.")
    }

    if *output == "" {
        log.Fatalf("Error -:- No output provided.")
    }

    f, err := sequtils.LoadFasta(*input)
    if err != nil {
        log.Fatalf("Error -:- sequtils.LoadFasta : %v\n", err)
    }

    fIdx, err := f.Index()
    if err != nil {
        log.Fatalf("Error -:- Index : %v\n", err)
    }

    lCoord, err := LoadSubseqs(*list, *sep, *zero)
    if err != nil {
        log.Fatalf("Error -:- LoadSubseqs : %v\n", err)
    }

    nFasta := sequtils.NewFna()
    for seq, coor := range lCoord {
        for _, c := range coor {
            idxSeq, err := f.Get(seq, fIdx)
            if err != nil {
                log.Printf("Info -:- Get : %v (%v)\n", err, seq)
            }
            subseq, err := idxSeq.Subsequence(c.Start, c.End)
            if err != nil {
                log.Fatalf("Error -:- Subsequence : %v\n", err)
            }
            subseq.Rename(fmt.Sprintf("%v|Start:%v|End:%v", subseq.Header, c.Start, c.End))
            nFasta.AddFasta(subseq)
        }
    }

    err = nFasta.Write(*output)
    if err != nil {
        log.Fatalf("Error -:- Write : %v\n", err)
    }
}